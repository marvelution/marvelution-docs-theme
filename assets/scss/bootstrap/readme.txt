This directory contains the bootstrap sources for scss/sass.

Get the sources here: https://getbootstrap.com/docs/5.0/getting-started/download/

Hugo doesn't really like the `vendor` directory so this should be renamed to `creator`.
