/**
 * Helper functions
 *
 */

// scroll to
function scrollTo(element) {
  window.scroll({
    behavior: "smooth",
    left: 0,
    top: element.offsetTop - 125,
  });
}

// get siblings
let getSiblings = function (e) {
  // for collecting siblings
  let siblings = [];
  // if no parent, return no sibling
  if (!e.parentNode) {
    return siblings;
  }
  // first child of the parent node
  let sibling = e.parentNode.firstChild;

  // collecting siblings
  while (sibling) {
    if (sibling.nodeType === 1 && sibling !== e) {
      siblings.push(sibling);
    }
    sibling = sibling.nextSibling;
  }
  return siblings;
};

// remove a class name from and array of dom elements
let removeClassNameFromArrEl = function (elArray, cssClass) {
  elArray.forEach(function (el) {
    el.classList.remove(cssClass);
  });
};
// ------- END helper functions

/**
 * Site header sticky logic
 */
const siteHeader = document.getElementById("site-header");

window.addEventListener("scroll", function () {
  makeHeaderStickyBasedOnScroll();
});

function makeHeaderStickyBasedOnScroll() {
  let scrollDistanceFromTop = window.scrollY;
  if (scrollDistanceFromTop > 10) {
    siteHeader.classList.add("is-sticky");
  } else {
    siteHeader.classList.remove("is-sticky");
  }
}

// run once onload
makeHeaderStickyBasedOnScroll();
/*----- END site header sticky logic -----*/

/**
 * Site header: mobile trigger
 */
const mobileMenuTrigger = document.getElementById("mobile-menu-trigger");

mobileMenuTrigger.addEventListener("click", function (e) {
  e.preventDefault();
  document.body.classList.add("sidebar-mobile-visible");
});
// ----- end site header mobile

/**
 * Home page service type selection logic
 */
window.serviceType = "";

const step1 = document.getElementById("stage-1");
const step2cloud = document.getElementById("stage-2-cloud");
const step2data = document.getElementById("stage-2-datacenter");

const serviceSelectBtn = document.getElementsByClassName("service-select-btn");
const sectionChooseDocPage = document.querySelector(
  ".section.section-choose-service"
);

for (const serviceSelectBtnElement of serviceSelectBtn) {
  serviceSelectBtnElement.addEventListener("click", function (e) {
    e.preventDefault();
    if (this.nextElementSibling) {
      this.nextElementSibling.classList.remove("selected");
    }
    if (this.previousElementSibling) {
      this.previousElementSibling.classList.remove("selected");
    }

    this.classList.toggle("selected");

    let selectedServiceType = this.dataset.service;
    // save selected service type for global use
    window.serviceType = selectedServiceType;
    console.log("selected service type: " + selectedServiceType);

    showCorrespondingDocLinks(selectedServiceType);

    document.querySelector(".active-stage").scrollIntoView();

    // if mobile scroll to newly visible section
    if (
      window.innerWidth <= 768 &&
      e.currentTarget.classList.contains("selected")
    ) {
      sectionChooseDocPage.scrollIntoView();
    }
  });
}

function showCorrespondingDocLinks(service) {
  if (service === "cloud") {
    if (step2cloud.classList.contains("visually-hidden")) {
      // show cloud doc links
      step2cloud.classList.remove("visually-hidden");
      step2data.classList.add("visually-hidden");
      // show indicator for step 2
      step2cloud.classList.add("active-stage");
      // hide indicator for step 1
      step1.classList.remove("active-stage");
    } else {
      // if already visible, hide
      step2cloud.classList.add("visually-hidden");
      // hide indicator fot step 2
      step2cloud.classList.remove("active-stage");
      // show indicator for step 1
      step1.classList.add("active-stage");
    }
  } else {
    if (step2data.classList.contains("visually-hidden")) {
      // show data-center-server doc links
      step2data.classList.remove("visually-hidden");
      step2cloud.classList.add("visually-hidden");
      // show indicator for step 2
      step2data.classList.add("active-stage");
      // hide indicator for step 1
      step1.classList.remove("active-stage");
    } else {
      // if already visible, hide
      step2data.classList.add("visually-hidden");
      // hide indicator for step 2
      step2data.classList.remove("active-stage");
      // show indicator for step 1
      step1.classList.add("active-stage");
    }
  }
}
// ----- END Home page service type selection logic

/**
 * Inner page sidebar logic
 * Open close and active state handle
 */
const sidebar = document.getElementById("sidebar");
// console.log(sidebar)

if (sidebar) {
  // --------- side bar show hide
  const sidebarToggle = document.getElementById("sidebar-collapse-trigger");
  sidebarToggle.addEventListener("click", function () {
    if (window.innerWidth <= 768) {
      document.body.classList.remove("sidebar-mobile-visible");
    } else {
      document.body.classList.toggle("sidebar-is-hidden");
    }
  });
}
// ---------- END inner page sidebar logic

/**
 * Inner page
 * Mobile floating cta
 */
const mobileFloatingCta = document.getElementById("mobile-floating-cta");
const mobileFloatingCtaClose = document.getElementById("mobile-cta-close");

// mobile cta depends on sessionStorage and not cookies
const floatingCtaAlreadyClosed = sessionStorage.getItem(
  "floating-cta-already-closed"
);

if (!floatingCtaAlreadyClosed) {
  mobileFloatingCta.classList.remove("visually-hidden");
} else {
  console.log("floating cta close btn not found");
}

if (mobileFloatingCta) {
  mobileFloatingCtaClose.addEventListener("click", function (e) {
    mobileFloatingCta.classList.add("visually-hidden");
    sessionStorage.setItem("floating-cta-already-closed", "yess");
  });
} else {
  console.log("mobile Floating Cta not available on this page");
}

/**
 * General code for
 * sidebar links
 * when used outside of sidebar
 */
const sideBarStyleLinkGroups = document.querySelectorAll(".sidebar-menu-links");
if (sideBarStyleLinkGroups.length) {
  sideBarStyleLinkGroups.forEach(function (menuItemGroup) {
    // --------- sidebar links
    const menuItems = menuItemGroup.querySelectorAll(".menu-item");
    const level_1_Links = menuItemGroup.querySelectorAll(".level-1");
    const level_2_Links = menuItemGroup.querySelectorAll(".level-2");

    // handle top level links
    level_1_Links.forEach(function (item) {
      item.addEventListener("click", function (e) {
        removeClassNameFromArrEl(level_1_Links, "active");
        removeClassNameFromArrEl(level_2_Links, "active");

        item.classList.add("active");
        // TODO goto corresponding page
      });
    });

    // handle sub menu menu links
    level_2_Links.forEach(function (item) {
      item.addEventListener("click", function (e) {
        removeClassNameFromArrEl(level_2_Links, "active");

        item.classList.add("active");
        // TODO goto corresponding page
      });
    });

    // handle cleanup
    menuItems.forEach(function (item) {
      item.addEventListener("click", function (e) {
        menuItems.forEach(function (el) {
          el.querySelector(".btn-link").classList.remove("active");
          let old_active_el = el.querySelector(".active");
          if (old_active_el) {
            el.querySelector(".active").classList.remove("active");
          }
        });
        item.querySelector(".btn-link").classList.add("active");

        // hide other open submenus
        let siblings = getSiblings(item);
        siblings.forEach(function (siblingEl) {
          if (siblingEl.classList.contains("has-sub-items")) {
            siblingEl
              .querySelectorAll('.level-1[aria-expanded="true"]')
              .forEach(function (x) {
                x.click();
              });
          }
        });
      });
    }); // foreach menu items
  }); // foreach menuGroups
}
// ---- end general code for sidebar

/**
 * Inner page nav links
 * on click respect navigation height
 */
const innerPageAnchorLinks = document.getElementById("current-page-nav-links");
if (innerPageAnchorLinks) {
  let links = innerPageAnchorLinks.querySelectorAll(".nav-link");
  links.forEach(function (link) {
    link.addEventListener("click", function (e) {
      e.preventDefault();

      // console.log(e.target.getAttribute("href"))
      // console.log(target.substring(1))

      let targetId = e.target.getAttribute("href").substring(1);

      scrollTo(document.getElementById(targetId));

      // if (target.scrollTop === to) return;
      // scrollTo(target, to, duration - 10);
    });
  });
}
// --- end inner page nav link
