function get(url) {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    req.open('GET', url);
    req.onload = () => req.status === 200 ? resolve(req.response) : reject(Error(req.statusText));
    req.onerror = (e) => reject(Error(`Network Error: ${e}`));
    req.send();
  });
}

const versionsDropdown = document.getElementById('versionsDropdown');
if (versionsDropdown) {
	const versionBaseUrl = versionsDropdown.getAttribute('data-version-baseurl');
	const currentVersion = versionsDropdown.getAttribute('data-current-version');

	get(`${versionBaseUrl}version-list`)
	.then(data => {
		versions = JSON.parse(data);
		if (versions.length > 0) {
			while (versionsDropdown.lastChild) {
		    versionsDropdown.removeChild(versionsDropdown.lastChild);
			}
			versionsDropdown.insertAdjacentHTML("beforeend", `<li><a class="dropdown-item disabled" href="#">Versions</a></li>`)

			versions.filter(version => version !== "cloud").forEach(version => {
				const active = version === currentVersion ? ' active' : '';
				const versionCapitalized = version.charAt(0).toUpperCase() + version.slice(1)
				versionsDropdown.insertAdjacentHTML("beforeend", `<li><a class="dropdown-item${active}" href="${versionBaseUrl}${version}">${versionCapitalized}</a></li>`);
			});
		}
	})
	.catch(err => {});
}
